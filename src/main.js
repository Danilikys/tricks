$(document).ready(function () {

  console.log('YEP');

  $('.slider-for').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    slidesPerRow: 3,
    asNavFor: '.slider-nav',
    dots: false,
    centerMode: true,
    focusOnSelect: true,
    centerPadding: '0px'
  });

  $('.slider-nav').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: '.slider-for'
  });

  $('.slider_main-item span').on('click', function () {

    $('.popup').fadeIn();

    if($('.popup').hasClass('onot')){

    } else{

      $('.slider-nav').slick('setPosition');

      $('.popup').addClass('onot');
    }

  });

  $('.popup__exit').on('click', function () {

    $('.popup').fadeOut();

  });

});
